var data = [1, 'firstString', 30, 500, true, true, null, 'abc', false, {test: 'Object'}, undefined],
    result;

result = prioritySort(data, ['number', 'null', 'string', 'object', 'undefined', 'boolean']);
//expected result [1, 30, 500, null, 'abc', 'firstString', {test: 'Object'}, undefined, true, true, false]

console.log('result', result);

function prioritySort(array, dataPriority) {
    var obj = {
        arr: [],
        arr0: [],
        arr1: [],
        arr2: [],
        arr3: [],
        arr4: [],
        arr5: []
    };
    function sortArray(arr, el) {
        function sortNum (a, b) {
            return a - b;
        }
        if (typeof el === 'number') {
            arr.sort(sortNum)
        } else if (typeof el === 'string') {
            arr.sort();
        }
    }
    array.forEach(function(el) {
        if (el === null) {
            obj['arr' + dataPriority.indexOf('null')].push(null);
        } else {
            switch (typeof el) {
                case dataPriority[0]:
                    obj.arr0.push(el);
                    sortArray(obj.arr0, el);
                    break;
                case dataPriority[1]:
                    obj.arr1.push(el);
                    sortArray(obj.arr1, el);
                    break;
                case dataPriority[2]:
                    obj.arr2.push(el);
                    sortArray(obj.arr2, el);
                    break;
                case dataPriority[3]:
                    obj.arr3.push(el);
                    sortArray(obj.arr3, el);
                    break;
                case dataPriority[4]:
                    obj.arr4.push(el);
                    sortArray(obj.arr4, el);
                    break;
                case dataPriority[5]:
                    obj.arr5.push(el);
                    sortArray(obj.arr5, el)
            }
        }
    });

    return obj.arr0.concat(obj.arr1, obj.arr2, obj.arr3, obj.arr4, obj.arr5)
}