var main = document.getElementById('main');
var input = document.getElementById('input');
var en = false;
kb();
function kb () {
    if (en) {
        main.innerHTML = '<div id="keyboard" class="keyboard">\n' +
            '    <div class="line">\n' +
            '        <div class="key" id="Backquote">~</div>\n' +
            '        <div class="key" id="Digit1">1</div>\n' +
            '        <div class="key" id="Digit2">2</div>\n' +
            '        <div class="key" id="Digit3">3</div>\n' +
            '        <div class="key" id="Digit4">4</div>\n' +
            '        <div class="key" id="Digit5">5</div>\n' +
            '        <div class="key" id="Digit6">6</div>\n' +
            '        <div class="key" id="Digit7">7</div>\n' +
            '        <div class="key" id="Digit8">8</div>\n' +
            '        <div class="key" id="Digit9">9</div>\n' +
            '        <div class="key" id="Digit0">0</div>\n' +
            '        <div class="key" id="Minus">-</div>\n' +
            '        <div class="key" id="Equal">=</div>\n' +
            '        <div class="key backspace letter-key" id="Backspace">Backspace</div>\n' +
            '    </div>\n' +
            '    <div class="line">\n' +
            '        <div class="key tab letter-key" id="Tab">Tab</div>\n' +
            '        <div class="key" id="KeyQ">Q</div>\n' +
            '        <div class="key" id="KeyW">W</div>\n' +
            '        <div class="key" id="KeyE">E</div>\n' +
            '        <div class="key" id="KeyR">R</div>\n' +
            '        <div class="key" id="KeyT">T</div>\n' +
            '        <div class="key" id="KeyY">Y</div>\n' +
            '        <div class="key" id="KeyU">U</div>\n' +
            '        <div class="key" id="KeyI">I</div>\n' +
            '        <div class="key" id="KeyO">O</div>\n' +
            '        <div class="key" id="KeyP">P</div>\n' +
            '        <div class="key" id="BracketLeft">[</div>\n' +
            '        <div class="key" id="BracketRight">]</div>\n' +
            '    </div>\n' +
            '    <div class="line">\n' +
            '        <div class="key capslock letter-key" id="CapsLock">Caps Lock</div>\n' +
            '        <div class="key" id="KeyA">A</div>\n' +
            '        <div class="key" id="KeyS">S</div>\n' +
            '        <div class="key" id="KeyD">D</div>\n' +
            '        <div class="key" id="KeyF">F</div>\n' +
            '        <div class="key" id="KeyG">G</div>\n' +
            '        <div class="key" id="KeyH">H</div>\n' +
            '        <div class="key" id="KeyJ">J</div>\n' +
            '        <div class="key" id="KeyK">K</div>\n' +
            '        <div class="key" id="KeyL">L</div>\n' +
            '        <div class="key" id="Semicolon">;</div>\n' +
            '        <div class="key" id="Quote">\'</div>\n' +
            '        <div class="key" id="Backslash">\\</div>\n' +
            '    </div>\n' +
            '    <div class="line">\n' +
            '        <div class="key lshift letter-key" id="ShiftLeft">Shift</div>\n' +
            '        <div class="key" id="KeyZ">Z</div>\n' +
            '        <div class="key" id="KeyX">X</div>\n' +
            '        <div class="key" id="KeyC">C</div>\n' +
            '        <div class="key" id="KeyV">V</div>\n' +
            '        <div class="key" id="KeyB">B</div>\n' +
            '        <div class="key" id="KeyN">N</div>\n' +
            '        <div class="key" id="KeyM">M</div>\n' +
            '        <div class="key" id="Comma">,</div>\n' +
            '        <div class="key" id="Period">.</div>\n' +
            '        <div class="key" id="Slash">/</div>\n' +
            '        <div class="key rshift letter-key" id="ShiftRight">Shift</div>\n' +
            '    </div>\n' +
            '    <div class="line">\n' +
            '        <div class="key lctrl letter-key" id="ControlLeft">Ctrl</div>\n' +
            '        <div class="key letter-key">Fn</div>\n' +
            '        <div class="key" id="MetaLeft"><img src="./img/w.png" class="window">\n' +
            '        </div>\n' +
            '        <div class="key letter-key" id="AltLeft">Alt</div>\n' +
            '        <div class="key space" id="Space">Space</div>\n' +
            '        <div class="key letter-key" id="AltRight">Alt</div>\n' +
            '        <div class="key letter-key">Fn</div>\n' +
            '        <div class="key" id="ContextMenu"><img src="./img/c.png" class="window">\n' +
            '        </div>\n' +
            '        <div class="key rctrl letter-key" id="ControlRight">Ctrl</div>\n' +
            '    </div>\n' +
            '    <div class="key enter letter-key" id="Enter">Enter</div>\n' +
            '</div>'
    }
    else {
        main.innerHTML = '<div id="keyboard" class="keyboard">\n' +
            '    <div class="line">\n' +
            '        <div class="key" id="Backquote">~</div>\n' +
            '        <div class="key" id="Digit1">1</div>\n' +
            '        <div class="key" id="Digit2">2</div>\n' +
            '        <div class="key" id="Digit3">3</div>\n' +
            '        <div class="key" id="Digit4">4</div>\n' +
            '        <div class="key" id="Digit5">5</div>\n' +
            '        <div class="key" id="Digit6">6</div>\n' +
            '        <div class="key" id="Digit7">7</div>\n' +
            '        <div class="key" id="Digit8">8</div>\n' +
            '        <div class="key" id="Digit9">9</div>\n' +
            '        <div class="key" id="Digit0">0</div>\n' +
            '        <div class="key" id="Minus">-</div>\n' +
            '        <div class="key" id="Equal">=</div>\n' +
            '        <div class="key backspace letter-key" id="Backspace">Backspace</div>\n' +
            '    </div>\n' +
            '    <div class="line">\n' +
            '        <div class="key tab letter-key" id="Tab">Tab</div>\n' +
            '        <div class="key" id="KeyQ">Й</div>\n' +
            '        <div class="key" id="KeyW">Ц</div>\n' +
            '        <div class="key" id="KeyE">У</div>\n' +
            '        <div class="key" id="KeyR">К</div>\n' +
            '        <div class="key" id="KeyT">Е</div>\n' +
            '        <div class="key" id="KeyY">Н</div>\n' +
            '        <div class="key" id="KeyU">Г</div>\n' +
            '        <div class="key" id="KeyI">Ш</div>\n' +
            '        <div class="key" id="KeyO">Щ</div>\n' +
            '        <div class="key" id="KeyP">З</div>\n' +
            '        <div class="key" id="BracketLeft">Х</div>\n' +
            '        <div class="key" id="BracketRight">Ъ</div>\n' +
            '    </div>\n' +
            '    <div class="line">\n' +
            '        <div class="key capslock letter-key" id="CapsLock">Caps Lock</div>\n' +
            '        <div class="key" id="KeyA">Ф</div>\n' +
            '        <div class="key" id="KeyS">Ы</div>\n' +
            '        <div class="key" id="KeyD">В</div>\n' +
            '        <div class="key" id="KeyF">А</div>\n' +
            '        <div class="key" id="KeyG">П</div>\n' +
            '        <div class="key" id="KeyH">Р</div>\n' +
            '        <div class="key" id="KeyJ">О</div>\n' +
            '        <div class="key" id="KeyK">Л</div>\n' +
            '        <div class="key" id="KeyL">Д</div>\n' +
            '        <div class="key" id="Semicolon">Ж</div>\n' +
            '        <div class="key" id="Quote">Э</div>\n' +
            '        <div class="key" id="Backslash">\\</div>\n' +
            '    </div>\n' +
            '    <div class="line">\n' +
            '        <div class="key lshift letter-key" id="ShiftLeft">Shift</div>\n' +
            '        <div class="key" id="KeyZ">Я</div>\n' +
            '        <div class="key" id="KeyX">Ч</div>\n' +
            '        <div class="key" id="KeyC">С</div>\n' +
            '        <div class="key" id="KeyV">М</div>\n' +
            '        <div class="key" id="KeyB">И</div>\n' +
            '        <div class="key" id="KeyN">Т</div>\n' +
            '        <div class="key" id="KeyM">Ь</div>\n' +
            '        <div class="key" id="Comma">Б</div>\n' +
            '        <div class="key" id="Period">Ю</div>\n' +
            '        <div class="key" id="Slash">.</div>\n' +
            '        <div class="key rshift letter-key" id="ShiftRight">Shift</div>\n' +
            '    </div>\n' +
            '    <div class="line">\n' +
            '        <div class="key lctrl letter-key" id="ControlLeft">Ctrl</div>\n' +
            '        <div class="key letter-key">Fn</div>\n' +
            '        <div class="key" id="MetaLeft"><img src="./img/w.png" class="window">\n' +
            '        </div>\n' +
            '        <div class="key letter-key" id="AltLeft">Alt</div>\n' +
            '        <div class="key space" id="Space">Space</div>\n' +
            '        <div class="key letter-key" id="AltRight">Alt</div>\n' +
            '        <div class="key letter-key">Fn</div>\n' +
            '        <div class="key" id="ContextMenu"><img src="./img/c.png" class="window">\n' +
            '        </div>\n' +
            '        <div class="key rctrl letter-key" id="ControlRight">Ctrl</div>\n' +
            '    </div>\n' +
            '    <div class="key enter letter-key" id="Enter">Enter</div>\n' +
            '</div>';
    }
}
document.addEventListener('keydown', function(event) {
    document.getElementById(event.code).style.backgroundColor = '#0B9064';
    input.value += event.key;
    if(event.code === 'Backspace') {
        input.value = input.value.slice(0, -10);
    }
    if (event.code === 'AltLeft' && event.shiftKey === true) {
        en = !en;
        kb();
    }
});

document.addEventListener('keyup', function(event) {
    document.getElementById(event.code).style.backgroundColor = '';
});