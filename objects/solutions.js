function query(collection) {
    var num = 0;
    if(arguments.length > 1) {
        num = arguments.length - 1;
    }
    var show = new Array(num);
    for (var i = 1; i < arguments.length; i++) {
        show[i - 1] = arguments[i];
    }
    for (var q = show.length; q--;) {
        collection = show[q](collection);
    }
    return collection;
}

function select() {
    var fields = [];
    for (var j = 0; j < arguments.length; j++) {
        fields.push(arguments[j]);
    }
    return function (arr) {
        return arr.map(function (x) {
            var obj = {};
            for (var k = 0; k < fields.length; k++) {
                var f = fields[k];
                obj[f] = x[f];
                if (!obj[f]) {
                    delete obj[f];
                }
            }
            return obj;
        });
    };
}

function filterIn(property, values) {
    return function (arr) {
        return arr.filter(function (item) {
            return values.indexOf(item[property]) >= 0;
        });
    };
}

module.exports = {
    timeShift: function(date) {
        var obj = {
            value: date,
            result: new Date(date),
            add: function(num, unit){
                switch (unit) {
                    case 'years': this.result.setYear(this.result.getFullYear() + num) ;
                        break;
                    case 'months': this.result.setMonth(this.result.getMonth() + num);
                        break;
                    case 'days': this.result.setDate(this.result.getDate() + num);
                        break;
                    case 'hours': this.result.setUTCHours(this.result.getHours() + num);
                        break;
                    case 'minutes': this.result.setMinutes(this.result.getMinutes() + num);
                        break;
                }
                this.value = this.result.toISOString().slice(0, 10) + ' ' + this.result.toISOString().slice(11, 16);
                return obj;
            },
            subtract: function(num, unit){
                switch (unit) {
                    case 'years': this.result.setYear(this.result.getFullYear() - num) ;
                        break;
                    case 'months': this.result.setMonth(this.result.getMonth() - num);
                        break;
                    case 'days': this.result.setDate(this.result.getDate() - num);
                        break;
                    case 'hours': this.result.setUTCHours(this.result.getHours() - num);
                        break;
                    case 'minutes': this.result.setMinutes(this.result.getMinutes() - num);
                        break;
                }
                this.value = obj.result.toISOString().slice(0, 10) + ' ' + obj.result.toISOString().slice(11, 16);
                return obj;
            }
        };
        return obj;
    },
    lib: {
        query: query,
        select: select,
        filterIn: filterIn
    }
};