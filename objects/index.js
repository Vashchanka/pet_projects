function query(collection) {
    var num = 0;
    if(arguments.length > 1) {
        num = arguments.length - 1;
    }
    var show = new Array(num);
    for (var i = 1; i < arguments.length; i++) {
        show[i - 1] = arguments[i];
    }
    for (var q = show.length; q--;) {
        collection = show[q](collection);
    }
    return collection;
}

function select() {
    var fields = [];
    for (var j = 0; j < arguments.length; j++) {
        fields.push(arguments[j]);
    }
    return function (arr) {
        return arr.map(function (x) {
            var obj = {};
            for (var k = 0; k < fields.length; k++) {
                var f = fields[k];
                obj[f] = x[f];
                if (!obj[f]) {
                    delete obj[f];
                }
            }
            return obj;
        });
    };
}

function filterIn(property, values) {
    return function (arr) {
        var result = arr.filter(function (item) {
            return values.indexOf(item[property]) >= 0;
        });
        console.log(result);
        return result;
    };
}