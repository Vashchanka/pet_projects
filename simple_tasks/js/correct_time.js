function isTimeValid(hours, minutes) {
    if (hours === '' || minutes === '') {
        return false;
    }
    return (0 <= hours && hours < 24) && (0 <= minutes && minutes <= 59);
}