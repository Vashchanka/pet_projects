function addMinutes(prevHours, prevMinutes, addNum) {
    var hour,
        min,
        sum = addNum + prevMinutes;
   if (sum >= 60) {
        var addHours = Math.floor(addNum / 60);
       min = addNum % 60 + prevMinutes;
        if (addHours < 1) {
            hour = ++prevHours;
        } else {
            hour = prevHours + addHours;
        }
       if (min <= 60 && addHours === 0) {
           min = Math.abs(prevMinutes + addNum - 60);
       }
       if (min >= 60 ) {
           min -= 60;
           if (addHours !== 0) {
               ++hour;
           }
       }
   } else {
       hour = prevHours;
       min = prevMinutes + addNum;
   }
    while (hour >= 24) {
        hour -= 24;
    }
   if (hour.toString().length === 1) {
       hour = '0' + hour;
   }
   if (min.toString().length === 1){
       min = '0' + min;
   }
    return hour + ':' + min;
}