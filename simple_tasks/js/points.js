function isPointInCircle(x, y) {
    return (x - 3) * (x - 3) + (y - 5) * (y - 5) <= 16;
}

function isPointInQuadrangle(x, y) {
    // y = kx + b
    // k = (y1 - y2) / (x1 - x2)
   // b = y2 - k*x2
var l1,
    l2,
    l3,
    l4;
l1 = (-3 / 5) * x + 3;
l2 = (4 / 7) * x + 4;
l3 = (2 / 5) * x - 2;
l4 = (-12 / 8) * x -12;

return (y <= l1 && y >= l3 && y <= l2 && y >= l4);
}