function factorial(x) {
    return x ? x * factorial(--x) : 1;
}