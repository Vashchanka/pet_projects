function getDayDeclension(days) {
    days = String(days);
    if (days == 1) {
        return 'День';
    } else if (2 <= days && days <= 4) {
        return 'Дня';
    } else if (days.substr(days.length - 2) >= 11 && days.substr(days.length - 2) <= 19 || 5 <= days && days <= 20 || days[days.length - 1] === '0') {
        return 'Дней';
    } else if ((days.length >= 2) && (days[days.length - 1] == 1)) {
        return 'День';
    } else if ((days.length >= 2) && (2 <= days[days.length - 1] && days[days.length - 1] <= 4)) {
        return 'Дня';
    } else if ((days.length >= 2) && (5 <= days[days.length - 1] && days[days.length - 1] <= 20)) {
        return 'Дней';
    } else {
        return 'Введите корректное число дней';
    }
}