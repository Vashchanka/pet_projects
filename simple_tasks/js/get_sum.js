function getSum(num) {
    /*
    var sum = 0;
    for (i=1; i <= num; i++) {
        sum += i;
    }
     */
    if (num !== 1) {
       return num + getSum(--num);
    } else {
        return 1;
    }
}