function getSeason(month) {
    if (month === 1 || month === 2 || month === 12) {
        return 'Зима';
    } else if (3 <= month && month <= 5) {
        return 'Весна';
    } else if (6 <= month && month <= 8) {
        return 'Лето';
    } else if (9 <= month && month <= 11) {
        return 'Осень';
    } else {
        return 'Введите корректное число от 1 до 12';
    }
}