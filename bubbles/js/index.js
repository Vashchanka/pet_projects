/**
 * DOM elements and variables
 * @type {Element}
 */
var stopBtn = document.getElementById('stop'),
    startBtn = document.getElementById('start'),
    main = document.getElementById('main'),
    score = document.createElement('h3'),
    menu = document.getElementById('menu'),
    timeDisplay = document.createElement('h3'),
    timeCount = 15,
    count = 0,
    speed = 0,
    sound = new Audio('./mp3/b.mp3');
startBtn.addEventListener('click', start);
menu.appendChild(timeDisplay);
menu.appendChild(score);

/**
 * Prevents selecting elements
 */
document.addEventListener('selectstart', function(e) {
    e.preventDefault();
});

/**
 * Random speed of bubble movement
 * @returns {number}
 */
function randomSpeed() {
    var rdm = 30 + Math.random() * 20;
    return speed + 40 - rdm;
}

/**
 * Creates a bubble
 * @returns {Element}
 * @constructor
 */
function Bubble () {
    var bubble = document.createElement('img');
    var pos = 700;
    bubble.src = 'img/bubble.png';
    bubble.style.position = 'absolute';
    bubble.style.borderRadius = '70px';
    bubble.style.left = (Math.random() * 800 + 200) + 'px';
    bubble.style.opacity='0.6';
    bubble.id = 'b';
    bubble.stop = function() {
        clearInterval(this.up)
    };
    bubble.up = setInterval(function() {
        bubble.style.top = pos + 'px';
        main.appendChild(bubble);
        if (pos === 0) {
            clearInterval(bubble.up);
            main.removeChild(bubble)
        }
        pos--;
    }, randomSpeed());
    bubble.addEventListener('mousedown', function() {
        count++;
        clearInterval(this.up);
        main.removeChild(bubble);
        sound.play();
        score.innerHTML = 'Счет: ' + count;
    });
    return bubble;
}

/**
 * Bubble starts moving
 */
function move(){
    var bubble = new Bubble();
    bubble.up;
}

/**
 * Handles user`s speed choice and starts a game
 * @returns {boolean}
 */
function start(){
    /**
     * Speed choice
     * @type {Element}
     */
    var chooseSpeed = document.querySelector('input:checked');
    if(!chooseSpeed){
        alert('Выберите скорость');
        return false
    } else {
        switch (chooseSpeed.id) {
            case 'slow': speed = 20;
                break;
            case 'normal': speed = 10;
                break;
            case 'fast': speed = 0;
        }
    }
    count = 0;
    timeCount = 15;
    /**
     * Counter of time left
     * @type {number}
     */
    var timeLeft = setInterval(function() {
        timeCount--;
        timeDisplay.innerHTML = 'Осталось ' + timeCount + ' сек.';
        if (timeCount < 5) {
            timeDisplay.style.color = 'red';
        }
        /**
         * If time is out, game stops and the score is shown
         */
        if (timeCount <= 0) {
            clearInterval(timeLeft);
            while(main.childElementCount) {
                var b = document.getElementById('b');
                clearInterval(start);
                b.stop();
                main.removeChild(b)
            }
            var modalWindow = document.getElementById('modalContent');
            modalWindow.innerText = 'Ваш счет: ' + count;
            $('#modal').modal('show');
        }
    }, 1000);
    /**
     * Time interval for bubbles to appear
     * @type {number}
     */
    var start = setInterval(function() {
        move()
    }, 700);
    var clear = function() {
        clearInterval(start);
        clearInterval(timeLeft);
        timeCount = 0;
    };
    stopBtn.addEventListener('click', clear);
    move();
}


