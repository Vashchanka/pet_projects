var main = document.getElementById('contentArea');
var circle = document.createElement('div');
var square = document.createElement('div');
var cs1 = document.createElement('div');
var cs2 = document.createElement('div');
var choice = document.createElement('div');
var newEl;
var el;

function rdm () {
        return Math.floor(Math.random() * (256));
}
function clearClick() {
    square.clicked = false;
    square.style.backgroundColor = '';
    circle.clicked = false;
    circle.style.backgroundColor = '';
    cs1.clicked = false;
    cs1.style.backgroundColor = '';
    cs2.clicked = false;
    cs2.style.backgroundColor = '';
}
square.style.width = '30px';
square.style.height = '30px';
square.style.border = 'solid black';
square.style.margin = '5px';
square.style.display = 'inline-block';
square.onclick = function(){
    clearClick();
    square.clicked = true;
    square.style.backgroundColor = 'black';
};
circle.style.width = '30px';
circle.style.height = '30px';
circle.style.border = 'solid black';
circle.style.margin = '5px';
circle.style.borderRadius = '50%';
circle.style.display = 'inline-block';
circle.onclick = function(){
    clearClick();
    circle.clicked = true;
    circle.style.backgroundColor = 'black';
};
cs1.style.width = '30px';
cs1.style.height = '30px';
cs1.style.border = 'solid black';
cs1.style.margin = '5px';
cs1.style.borderRadius = '0 40% 0 40%';
cs1.style.display = 'inline-block';
cs1.onclick = function(){
    clearClick();
    cs1.clicked = true;
    cs1.style.backgroundColor = 'black';
};
cs2.style.width = '30px';
cs2.style.height = '30px';
cs2.style.border = 'solid black';
cs2.style.margin = '5px';
cs2.style.borderRadius = '40% 0 40% 0';
cs2.style.display = 'inline-block';
cs2.onclick = function(){
    clearClick();
    cs2.clicked = true;
    cs2.style.backgroundColor = 'black';
};
var form = document.createElement('form');
var input = document.createElement('input');
input.type='TEXT';
input.id='input';
var clear = document.createElement('button');
clear.onclick = function(){
    location.reload();
};
clear.innerText = 'Очистить';
var submit = document.createElement('button');
submit.type='button';
submit.innerHTML = 'Отправить';
submit.id = 'submit';
submit.onclick = function () {
    var val = +document.getElementById('input').value;
    if (val > 0 && val <= 30 && parseInt(val) === val) {
        if (square.clicked) {
            el = square;
        } else if (circle.clicked) {
            el = circle;
        } else if (cs1.clicked) {
            el = cs1;
        } else if (cs2.clicked) {
            el = cs2;
        } else {
            alert('Выберите форму фигуры');
            return false;
        }
        for (var j = 0; j < 3; j++) {
            var block = document.createElement('div');
            for (var i = 0; i < 3; i++) {
                newEl = el.cloneNode(false);
                newEl.style.border = 'none';
                newEl.style.width = val + 'px';
                newEl.style.height = val + 'px';
                newEl.style.backgroundColor = 'rgb(' + rdm() + ',' + rdm() + ',' + rdm() + ')';
                block.appendChild(newEl);
            }
            main.appendChild(block);
        }
    } else {
        alert('Введите целое число от 0 до 30');
        return false;
    }
};
form.appendChild(input);
form.appendChild(submit);
form.appendChild(clear);
main.appendChild(form);
choice.appendChild(square);
choice.appendChild(circle);
choice.appendChild(cs1);
choice.appendChild(cs2);
main.appendChild(choice);



