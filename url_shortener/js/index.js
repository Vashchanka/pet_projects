var num = 0;
var data = [];

var goButton = document.createElement('button');
goButton.className = 'btn btn-primary';
goButton.innerHTML = 'GO TO';
goButton.style.width = '80px';
goButton.style.height = '30px';
var delButton = document.createElement('button');
delButton.className = 'btn btn-danger';
delButton.innerHTML = 'REMOVE';
delButton.style.width = '80px';
delButton.style.height = '30px';

function clearTable() {
    var trOld = document.getElementsByClassName('trNew');
    while(trOld[0])
        trOld[0].parentNode.removeChild(trOld[0]);
}

function short(url) {
    return 'http://short.com/' + Math.random().toString(32).substring(2, 7);
}

function openUrl(el) {
    var url;
    for(var key in data) {
        if(data[key].ShortURL === el) {
            url = data[key].URL;
        }
    }
    window.open(url);
}

function show() {
    var table = document.querySelector('tbody');
    var td = document.createElement('td');
    data.forEach(function(el) {
        var tr = document.createElement('tr');
        tr.className = 'trNew';
        for (var i in el) {
            td = document.createElement('td');
            td.appendChild(document.createTextNode(el[i]));
            tr.appendChild(td);
        }
        var goButton = document.createElement('button');
        goButton.className = 'btn btn-primary';
        goButton.innerHTML = 'GO TO';
        goButton.style.width = '80px';
        goButton.style.height = '30px';
        var delButton = document.createElement('button');
        delButton.className = 'btn btn-danger';
        delButton.innerHTML = 'REMOVE';
        delButton.style.width = '80px';
        delButton.style.height = '30px';
        delButton.onclick = function(e) {
            e.preventDefault();
            alert('Запись, оставленная ' + el.Name + ', удалена');
            data.splice(data.indexOf(el), 1);
            clearTable();
            show();
        };
        goButton.onclick = function(e) {
            e.preventDefault();
            openUrl(el.ShortURL);
        };
        td = document.createElement('td');
        tr.appendChild(td);
        td.appendChild(goButton);
        td = document.createElement('td');
        tr.appendChild(td);
        td.appendChild(delButton);
        table.appendChild(tr)
    });
}

function addUrl(name, url) {
    num++;
    if(url.search(/^https?:\/\//) !== 0) {
        url = 'http://' + url;
    }
    clearTable();
    data.push({
            "#": num,
            "Name": name,
            "URL": url,
            "ShortURL": short(url)
    });
    show();
}