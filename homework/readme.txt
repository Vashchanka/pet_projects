1. ����� �������
���������� �������� �������, ������� ��������� ���� �������� � ������ � ���������� ������ ��������, ������� ���������� � ������. ���� ������� (#) ���������� �������.
��������:
getWords(�������� ���� � ������� #intexsoft �� #javascript�) � ������� ������ ������� [�intexsoft�, � javascript�];

2. ������ ������������� �����
���������� �������� �������, ������� ��������� ���� �������� � ������ �� ������� ���� � ���������� ������, ������� �� �������� ������������� ����, � ��� ����� � ���� ������ ��������� � ������� ��������. ��� ����� � �������� ������ ������ ���� ��������� ������� � ��������.
��������:
normalizeWords(['web', 'intexsoft', 'JavaScript', 'Intexsoft', 'script', 'programming'])
������ ������� ������: "web, intexsoft, javascript, script, programming" (��� ����� � ������ ��������, ����������� ������� � ��������, ��� ������������� ����)

3. �������� �����
� ���� ������� ���������� ����������� �������, ����� ������� ����� ��������� ���������� ������.
��� ���������� ���������� ������ ����� ����������� ��� �������:
�	ADD � ��������� �������
�	REMOVE_PHONE � ������� �����
�	SHOW � ���������� ���������� ���������� �����
addressBook('ADD Ivan 555-10-01,555-10-03');
addressBook('ADD Ivan 555-10-02');
console.info(addressBook ('SHOW'));
// �����:
// ["Ivan: 555-10-01, 555-10-03, 555-10-02"]
addressBook('REMOVE_PHONE 555-10-03');
addressBook('ADD Alex 555-20-01');
addressBook('SHOW');
// �����:
// ["Alex: 555-20-01", "Ivan: 555-10-01, 555-10-02"]
addressBook('REMOVE_PHONE 555-20-01');
addressBook('SHOW');
// �����:
// ["Ivan: 555-10-01, 555-10-02"]
�������������, ��� ������� ����� ���������� ���������, ������ �� ������� ������������� ������. ������������ ������� ������ ��������� �� �����.
��� ������� ������� �������� �������, ��������� ����������� ����� ��������.
������������� ������������ ����������� ���������.
������� ADD
��������� ������� � ���������� ����� �� ������� ���������. �������� ������������� ����� �������. ���� ����� ������� ����������, �� ������� ��������� ������ ��������� ��������.
ADD Name phone1,phone2
������� REMOVE_PHONE
������� ������� �� ���������� �����. ���� ������� ������� �����, �� ������� ������ ������� true. ���� ������ �������� � ���������� ����� �� ����������, �� ������������ false.
REMOVE_PHONE phone1
������� SHOW
���������� ������ ��������� � �� ����������. ������ �������� ������� ����: "���: �������1, �������2". ������ ������ ���� ������������ �� ����� ��������. �������� ���� � ������� ���������� �� � ���������� �����. ������� � ������ ������� ��������� �� ������ ������������.
ADD Contact2 phone4,phone3
ADD Contact1 phone1
ADD Contact1 phone2
SHOW
[
	"Contact1: phone1, phone2",
	"Contact2: phone4, phone3"
]

