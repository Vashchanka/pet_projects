var phoneBook = [];

var searchName = function(name, phoneBook) {
   for (var i=0; i < phoneBook.length; i++) {
       if (phoneBook[i].match(name)) {
           return i;
       }
   }
};

var searchPhone = function(phone, phoneBook) {
    for (var i=0; i < phoneBook.length; i++) {
        if (phoneBook[i].match(phone)) {
            return i;
        }
    }
};

var hasNumbers = function(num, phoneBook) {
    for (var i=0; i < phoneBook.length; i++) {
        if (phoneBook[i].match(num)) {
            return i;
        }
    }
};

function filterNumbers (number, phoneBook) {
    var joined = phoneBook.join();
    var result = [];
    for (var i = 0; i < number.length; i++ ) {
        if (joined.match(number[i]) === null) {
            result.push(number[i]);
        }
        }
    return result;
}

function ADD(name, phone){
    var nameExists = searchName(name, phoneBook);
    var phoneExists = searchPhone(phone, phoneBook);
    if (nameExists === undefined && phoneExists === undefined) {
        var newContact = name + ' ' + phone.join(', ');
        phoneBook.push(newContact);
    } else {
        var num = hasNumbers(/\d+/, phoneBook);
        if (num === undefined) {
            phoneBook[nameExists] += phone;
        } else {
            var add = '' + filterNumbers(phone, phoneBook);
            if (add !== '') {
                phoneBook[nameExists] += ', ' + add;
            }
        }}

}

function REMOVE_PHONE(phone){
    var phoneExists = searchPhone(phone, phoneBook);
    var result;
    if (phoneExists === undefined) {
        result = false;
    } else {
        phoneBook[phoneExists] = phoneBook[phoneExists].replace(phone, '');
        phoneBook[phoneExists] = phoneBook[phoneExists].replace(' , ', ' ');
        if (phoneBook[phoneExists].substr(-2) === ', ') {
            phoneBook[phoneExists] = phoneBook[phoneExists].slice(0, -2);
        }
        result = true;
    }
    console.log(result);
    return result;
}

function SHOW(){
    var show = phoneBook.sort().filter(function(item, i, arr){
    return item.search(/\d+/) !== -1;
    });
    console.log(show);
    return show;
}

module.exports = {
    getWords: function(sentence){
var arr = sentence.split(' ');
var newArr = [];
arr.filter(function(item) {
   return item.indexOf('#') !== -1;
}).forEach(function(item){
    item = item.slice(1);
    newArr.push(item);
});
console.log(newArr);
    return newArr;
},
    normalizeWords: function(words){
var newArr = [];
words.forEach(function(item){
    item = item.toLowerCase();
    newArr.push(item);
    return newArr;
});
var str = newArr.filter(function(item, i, arr){
    return arr.indexOf(item) === i;
}).join(', ');
console.log(str);
    return str;
},
    addressBook: function(command){
var arr = command.split(' ');
    var action = arr[0];
    var name = arr[1] + ':';
    if (action === "ADD") {
        var phone = arr[2].split(',');
    ADD(name, phone)
} else if (action === "REMOVE_PHONE") {
    phone = arr[1];
    REMOVE_PHONE(phone)
} else if (action === "SHOW"){
    SHOW();
} else {
    console.log('unknown action')
}
}
}
