function game () {
    var moves = 0;
    var displayMoves = document.createElement('div');
    var rows = document.getElementById('rows');
    var n = +rows.options[rows.selectedIndex].value;
    var main = document.getElementById('main');
    var block;
    main.style.height = n * 52 + 'px';
    main.style.width = n * 52 + 'px';
    var arr = [];
    var amount = n * n - 1;
    main.innerHTML = '';

    function isSorted(arr) {
        for(var i = 0; i < amount-1; i++) {
            if(arr[i] > arr[i+1]) {
                return false;
            }
        }
        return true;
    }
    function rdmPush() {
        for (var i = 0; i < amount; i++) {
            var rdm;
            do {
                rdm = Math.ceil(Math.random() * amount);
                console.log('Предлагаю ' + rdm);
            }
            while (arr.indexOf(rdm) !== -1);
            console.log('Будет добавлен ' + rdm);
            arr.push(rdm)
        }
        arr.push(' ');
    }

    rdmPush();
    var move = function (id) {
        var toReplace = arr.indexOf(' ');
        var inner = arr[id];
        console.log('массив ' + arr);
        if (Math.abs(toReplace - id) === 1 || Math.abs(toReplace - id) === n) {
            arr.splice(id, 1, ' ');
            arr.splice(toReplace, 1, inner);
            moves++;
            refreshTable();
            displayMoves.innerHTML = 'Ходы: ' + moves;
            main.appendChild(displayMoves);
            if (isSorted(arr)) {
                alert('Вы выиграли за ' + moves + ' ходов')
            }
        } else {
            main.style.backgroundColor = 'red';
            setTimeout(function () {
                main.style.backgroundColor = 'white'
            }, 300)
        }
    };

    function refreshTable() {
        main.innerHTML = '';
        table();
    }

    function table() {
        var num = 0;
        for (var i = 0; i < n; i++) {
            for (var j = 0; j < n; j++) {
                block = document.createElement('div');
                block.style.width = '50px';
                block.style.height = '50px';
                block.id = num;
                block.style.border = '1px solid black';
                block.style.float = 'left';
                block.innerHTML = arr[num];
                if (arr[num] === ' ') {
                    block.style.backgroundColor = '#0cffb1';
                }
                block.onclick = function () {
                    move(this.id);
                };
                main.appendChild(block);
                num++;
            }
        }

    }

    table();

}