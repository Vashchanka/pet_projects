var xhr = new XMLHttpRequest(),
    adminLogin = 'admin',
    adminPassword = 'password',
    main = document.getElementById('main'),
    loginButton = document.getElementById('loginButton'),
    msgLogin = document.getElementById('msgLogin');
loginButton.onclick = login;

/**
 * Login form on main page
 */
function login () {
    var user = document.querySelector('input:checked');
    var login = document.getElementById('login').value;
    var password = document.getElementById('password').value;
    if (!login || !password) {
        msgLogin.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
            "  Введите логин и пароль\n" +
            "</div>";
    }
    if (!user) {
        msgLogin.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
            "  Отметьте один из 3 вариантов входа\n" +
            "</div>";
    } else {
        switch (user.id) {
            case 'admin': adminCheck(login, password);
                break;
            case 'doctor': doctorCheck(login, password);
                break;
            case 'patient': patientCheck(login, password);
        }
    }
}

/**
 * Administrator data check, proceeds to admin`s page if successful
 * @param login
 * @param password
 */
function adminCheck(login, password) {
    if (login === adminLogin && password === adminPassword) {
        adm();
    } else {
        msgLogin.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
            "  Неверный логин или пароль\n" +
            "</div>";
    }
}

/**
 * Doctor`s data check, proceeds to doctors`s page if successful
 * @param login
 * @param password
 */
function doctorCheck(login, password) {
    xhr = new XMLHttpRequest;
    xhr.open('POST', 'http://localhost:3000/doctors_login/', false);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            if(!xhr.responseText) {
                msgLogin.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
                    "  Неверный логин или пароль\n" +
                    "</div>";
            } else {
                doc(xhr.responseText)
            }
        }
    };
    if (!password) {
        xhr.send('login=' + login);
    } else {
        xhr.send('login=' + login + '&passDoc=' + password);
    }
}

/**
 * Patient data check, proceeds to patient`s page if successful
 * @param login
 * @param password
 */
function patientCheck(login, password) {
    xhr.open('POST', 'http://localhost:3000/patient_login', false);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
        if(xhr.readyState === 4 && xhr.status === 200) {
            isPatient(xhr.responseText)
        } else if (xhr.status === 404) {
            msgLogin.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
                "  Неверный логин или пароль\n" +
                "</div>";
        }
    };
    xhr.send('login=' + login + '&pass=' + password);
}

/**
 * Administrator`s page after successful login
 */
function adm() {
    main.innerHTML = "<h3>Здравствуйте, Администратор</h3>\n" +
    "<button id=\"doctors-list\" type=\"button\" class=\"btn ml-4 btn-info\">Список врачей</button>\n" +
    "<button id=\"doctors-add\" type=\"button\" class=\"btn ml-5 btn-info\">Добавить врача</button>";

    document.getElementById('doctors-list').addEventListener('click', showDoctors);
    document.getElementById('doctors-add').addEventListener('click', createDoctor);

}

/**
 * Doctor`s page after successful login
 * @param res - doctor`s data passed to other functions to show or create patients
 */
function doc(res) {
    var displayName = JSON.parse(res).name;
    main.innerHTML =  "<h3>Здравствуйте, "+ displayName +"</h3>\n" +
        "<button id=\"patients-list\" type=\"button\" class=\"btn btn-info\">Список пациентов</button>\n" +
        "<button id=\"patients-add\" type=\"button\" class=\"btn btn-info\">Добавить пациента</button>";

    document.getElementById('patients-list').addEventListener('click', function() {
        showPatients(res)
    });
    document.getElementById('patients-add').addEventListener('click', function(){
        createPatient(res)
    });
}

/**
 * Patient`s page after successful login
 * @param patient - patient`s data to fill in the table
 */
function isPatient(patient) {
    var data = JSON.parse(patient);
    main.innerHTML =
        "<h3>Здравствуйте, "+ data.name +"</h3>\n" +
        "<table id=\"patient\" class=\"table table-striped w-auto\">\n" +
        "    <thead>\n" +
        "    <tr>\n" +
        "        <th scope=\"col\">Лечащий врач</th>\n" +
        "        <th scope=\"col\">Логин</th>\n" +
        "        <th scope=\"col\">Пациент</th>\n" +
        "        <th scope=\"col\">Диагноз</th>\n" +
        "        <th scope=\"col\">Лекарства</th>\n" +
        "    </tr>\n" +
        "    </thead>\n" +
        "    <tbody>\n" +
        "    </tbody>\n" +
        "</table>";
    var table = document.querySelector('#patient');
    var td = document.createElement('td');
    var tr = document.createElement('tr');
    td.appendChild(document.createTextNode(data.doctor));
    tr.appendChild(td);
    td = document.createElement('td');
    td.appendChild(document.createTextNode(data.login_patient));
    tr.appendChild(td);
    td = document.createElement('td');
    td.appendChild(document.createTextNode(data.name));
    tr.appendChild(td);
    td = document.createElement('td');
    td.appendChild(document.createTextNode(data.diag));
    tr.appendChild(td);
    td = document.createElement('td');
    td.appendChild(document.createTextNode(data.drug));
    tr.appendChild(td);
    table.appendChild(tr);
}

/**
 * Form for adding a new doctor
 */
function createDoctor() {
    main.innerHTML = "<form class=\"createTab\">\n" +
        "    <label>Логин: <input class=\"create\" type=\"text\" placeholder=\"Логин\" name=\"login\"></label>\n" + "<br>" +
        "    <label>Пароль: <input class=\"create\" type=\"text\" placeholder=\"Пароль\" name=\"passDoc\"></label>\n" + "<br>" +
        "    <label>ФИО: <input class=\"create\" type=\"text\" placeholder=\"ФИО\" name=\"name\"></label>\n" + "<br>" +
        "    <label>Специализация: <input class=\"create\" type=\"text\" placeholder=\"Специализация\" name=\"spec\"></label>\n" + "<br>" +
        "    <button type=\"button\" class=\"btn btn-light ml-2 mb-1\" onclick=\"sendDoctor(this.form.login.value, this.form.passDoc.value, this.form.name.value, this.form.spec.value)\">Создать</button>\n" +
        "    <button type=\"button\" class=\"btn btn-light ml-2 mb-1\" onclick=\"adm()\">Назад</button>\n" +
        "</form>\n" + "<div id=\"msgCreate\"></div>"
}

/**
 * Form for adding a new patient
 * @param res - attending doctor`s data
 */
function createPatient (res) {
    var docLogin = JSON.parse(res).login,
        docPass = JSON.parse(res).passDoc;
    main.innerHTML = "<form class=\"createTab\">\n" +
        "        <label>Логин: <input class=\"create\" type=\"text\" placeholder=\"Логин\" name=\"login_patient\"></label>\n" + "<br>" +
        "        <label>Пароль: <input class=\"create\" type=\"text\" placeholder=\"Пароль\" name=\"passPat\"></label>\n" + "<br>" +
        "        <label>ФИО: <input class=\"create\" type=\"text\" placeholder=\"ФИО\" name=\"name\"></label>\n" + "<br>" +
        "        <label>Диагноз: <input class=\"create\" type=\"text\" placeholder=\"Диагноз\" name=\"diag\"></label>\n" + "<br>" +
        "        <label>Лекарства: <input class=\"create\" type=\"text\" placeholder=\"Лекарства\" name=\"drug\"></label>\n" + "<br>" +
        "        <button type=\"button\" class=\"btn btn-light ml-2 mb-1\" onclick=\"sendPatient('" + docLogin + "', this.form.login_patient.value, this.form.passPat.value, this.form.name.value, this.form.diag.value, this.form.drug.value)\">Создать</button>\n" +
        "        <button type=\"button\" class=\"btn btn-light ml-2 mb-1\" onclick=\"doctorCheck('" + docLogin + "','" + docPass + "')\">Назад</button>\n" +
        "</form>\n" + "<div id=\"msgCreate\"></div>";
}


/**
 * Sends new doctor data to server and handles server`s response
 * @param login - doctor`s data passed from doctor creation table
 * @param pass - doctor`s data passed from doctor creation table
 * @param name - doctor`s data passed from doctor creation table
 * @param spec - doctor`s data passed from doctor creation table
 * @returns {boolean} - Should not send a request if some fields are empty
 */
function sendDoctor(login, pass, name, spec){
    var msgCreate = document.getElementById('msgCreate');
    var check = [].slice.call(arguments).every(function(el){
        return el
    });
    if (!check) {
            msgCreate.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
                "  Заполните все поля\n" +
                "</div>";
            return false
        }
    xhr.open('POST', 'http://localhost:3000/doctors', false);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200 && xhr.responseText !== 'Login exists') {
            msgCreate.innerHTML = "<div class=\"alert alert-success\" role=\"alert\">\n" +
                "  Доктор успешно добавлен\n" +
                "</div>";
            setTimeout(adm, 2000);
        } else if (xhr.readyState === 4 && xhr.responseText === 'Login exists') {
            msgCreate.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
                "  Доктор с логином " + login + " уже существует\n" +
                "</div>";
        }
    };
    xhr.send('login=' + login + '&passDoc=' + pass + '&name=' + name + '&spec=' + spec);
}

/**
 * Sends new patient`s data to server and handles server`s response
 * @param doctor - attending doctor`s data
 * @param login - patient`s data passed from patient creation table
 * @param pass - patient`s data passed from patient creation table
 * @param name - patient`s data passed from patient creation table
 * @param diag - patient`s data passed from patient creation table
 * @param drug - patient`s data passed from patient creation table
 * @returns {boolean} if some fields are empty, should not send a request
 */
function sendPatient(doctor, login, pass, name, diag, drug){
    var msgCreate = document.getElementById('msgCreate');
    var check = [].slice.call(arguments).every(function(el){
        return el
    });
    if (!check) {
        msgCreate.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
            "  Заполните все поля\n" +
            "</div>";
        return false
    }
    xhr.onreadystatechange = function () {
        if(xhr.readyState === 4 && xhr.status === 200 && xhr.responseText !== 'Login exists') {
            msgCreate.innerHTML = "<div class=\"alert alert-success\" role=\"alert\">\n" +
                "  Пациент успешно добавлен\n" +
                "</div>";
            setTimeout(doctorCheck, 2000, doctor);
        } else if (xhr.readyState === 4 && xhr.responseText === 'Login exists') {
            msgCreate.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
                "  Пациент с логином " + login + " уже существует\n" +
                "</div>";
        }
    };
    xhr.open('POST', 'http://localhost:3000/patients/add', false);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send('doctor=' + doctor + '&login_patient=' + login + '&passPat=' + pass + '&name=' + name + '&diag=' + diag + '&drug=' + drug);

}

/**
 * A table displaying all doctors from Admin`s page
 */
function showDoctors() {
    xhr = new XMLHttpRequest;
    xhr.open('GET', 'http://localhost:3000/', false);
    xhr.send();
    var doctorsData = JSON.parse(xhr.responseText);
    main.innerHTML = "<h3>Список докторов</h3>\n" + "<table id=\"doctors\" class=\"table table-striped w-auto\">\n" +
        "    <thead>\n" +
        "    <tr>\n" +
        "        <th scope=\"col\">Логин</th>\n" +
        "        <th scope=\"col\">ФИО</th>\n" +
        "        <th scope=\"col\">Специализация</th>\n" +
        "        <th scope=\"col\">Пациенты</th>\n" +
        "        <th scope=\"col\">Удаление</th>\n" +
        "    </tr>\n" +
        "    </thead>\n" +
        "    <tbody>\n" +
        "    </tbody>\n" +
        "</table>";
    var table = document.querySelector('#doctors');
    var td = document.createElement('td');
    doctorsData.forEach(function (el) {
        var tr = document.createElement('tr');
        for (var i in el) {
            if (i === '_id') {
                delete i._id;
            } else if (i === 'passDoc') {
                delete i.passDoc;
            }  else if (i === 'patients') {
                td = document.createElement('td');
                td.appendChild(document.createTextNode(el.patients.length));
                tr.appendChild(td);
            }else {
                td = document.createElement('td');
                td.appendChild(document.createTextNode(el[i]));
                tr.appendChild(td);
            }
        }
        var delButton = document.createElement('button');
        delButton.className = "btn-sm btn-danger mt-2 ml-5";
        delButton.style.borderRadius = "50%";
        delButton.innerText = "x";
        delButton.onclick = function () {
            xhr.open('DELETE', 'http://localhost:3000/doctors/' + el._id, false);
            xhr.send();
            adm();
        };
        tr.appendChild(delButton);
        table.appendChild(tr);
    });
    var backButton = document.createElement('button');
    backButton.innerText = 'Назад';
    backButton.className = "btn btn-light";
    backButton.onclick = function () {
        adm();
    };
    main.appendChild(backButton);
}

/**
 * A table displaying patients of logged in doctor
 * @param doctor - attending doctor`s data
 */
function showPatients(doctor) {
    var arr = JSON.parse(doctor);
    main.innerHTML = "<h3>Список пациентов доктора " + arr.name + "</h3>\n" + "<table id=\"patients\" class=\"table table-striped w-auto\">\n" +
        "        <thead>\n" +
        "        <tr>\n" +
        "            <th scope=\"col\">Логин врача</th>\n" +
        "            <th scope=\"col\">Логин пациента</th>\n" +
        "            <th scope=\"col\">ФИО</th>\n" +
        "            <th scope=\"col\">Диагноз</th>\n" +
        "            <th scope=\"col\">Лекарства</th>\n" +
        "            <th scope=\"col\">Удаление</th>\n" +
        "        </tr>\n" +
        "        </thead>\n" +
        "        <tbody>\n" +
        "        </tbody>\n" +
        "    </table>";
    var table = document.querySelector('#patients');
    var td = document.createElement('td');
    arr.patients.forEach(function (el) {
        var tr = document.createElement('tr');
        for (var i in el) {
            if (i === 'passPat') {
                delete el.passPat
            } else {
                td = document.createElement('td');
                td.appendChild(document.createTextNode(el[i]));
                tr.appendChild(td);
            }
        }
        var delButton = document.createElement('button');
        delButton.className = "btn-sm btn-danger mt-2 ml-5";
        delButton.innerText = "x";
        delButton.style.borderRadius = "50%";
        delButton.onclick = function () {
            xhr.open('POST', 'http://localhost:3000/patients/del', false);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.send('doctor=' + el.doctor + '&login=' + el.login_patient);
            doctorCheck(arr.login, arr.passDoc)
        };
        tr.appendChild(delButton);
        table.appendChild(tr);
    });
    var backButton = document.createElement('button');
    backButton.innerText = 'Назад';
    backButton.className = "btn btn-light";
    backButton.onclick = function () {
        doctorCheck(arr.login, arr.passDoc)
    };
    main.appendChild(backButton);

}
