var express = require('express'),
     app = express(),
     bodyParser= require('body-parser'),
     port = 3000,
     mongodb = require('mongodb'),
     MongoClient = require('mongodb').MongoClient,
     db,
     cors = require('cors');

/**
 * Connection to database
 */
MongoClient.connect('mongodb://localhost:27017/doctors', { useNewUrlParser: true }, function(err, client) {
    if (err) {
        return console.log(err)
    }
    db = client.db('doctors');
    app.listen(port, function() {
        console.log('Готов к работе, порт ' + port)
    });
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

/**
 * Get all doctors
 */
app.get('/', function(req, res) {
    db.collection('doctors').find().toArray(function(err, results) {
        res.send(results);
})});

/**
 * Adding a new doctor
 */
app.post('/doctors', function (req, res) {
    req.body.patients = [];
    db.collection('doctors').findOne({'login':req.body.login}, function(err, result) {
        if (err) {
            return console.log(err)
        }
        if (!result) {
            db.collection('doctors').insertOne(req.body, function(err, result) {
                if (err) {
                    return console.log(err)
                }
                console.log('Врач добавлен');
                res.sendStatus(200);
            })
        } else {
            res.send('Login exists')
        }
    });

});

/**
 * Doctor`s login
 */
app.post('/doctors_login', function(req, res) {
    var param;
    if(!req.body.passDoc) {
        param = {login: req.body.login}
    } else {
        param = req.body
    }
    db.collection('doctors').findOne(param, function(err, result) {
        if (err) {
            return console.log(err)
        }
        res.send(result);
    })});

/**
 * Doctor removing
 */
app.delete('/doctors/:id', function(req, res) {
    db.collection('doctors').deleteOne({_id: new mongodb.ObjectID(req.params.id)}, function(err, result) {
        if (err) {
            return console.log(err)
        }
        res.send(result);
    })});

/**
 * Adding a new patient
 */
app.post('/patients/add', function (req, res) {
    /**
     * Search for patient with specified login
     */
    db.collection('doctors').findOne({'patients.login_patient': req.body.login_patient}, function(err, result) {
        if (err) {
            return console.log(err)
        }
        if (!result) {
            /**
             * If patient not found = add him
             */
        db.collection('doctors').updateOne({login: req.body.doctor}, {$push: {patients: req.body}}, function(err, result) {
            if (err) {
                return console.log(err)
            }
            console.log('Пациент добавлен');
            res.sendStatus(200);
        })
        } else {
            res.send('Login exists')
        }});

});

/**
 * Patient`s login
 */
app.post('/patient_login/', function(req, res) {
    db.collection('doctors').findOne({'patients.login_patient': req.body.login, 'patients.passPat': req.body.pass}, function(err, result) {
        if (err) {
            return console.log(err)
        }
        var pat;
        if (!result) {
            /**
             * Patient not found or invalid data
             */
            res.sendStatus(404)
        } else {
            result.patients.forEach(function(item){
                if(item.login_patient === req.body.login) {
                    pat = item;
                    item.doctor = result.name;
                }
            });
            res.send(pat);
        }})});

/**
 * Removing patient
 */
app.post('/patients/del', function(req, res) {
    db.collection('doctors').updateOne({login: req.body.doctor}, {$pull: {patients: {login_patient: req.body.login}}}, function(err, result) {
        if (err) {
            return console.log(err)
        }
        res.send(result);
    });

});


