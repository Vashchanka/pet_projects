var choice = document.createElement('div'),
    rookSelected = document.createElement('img'),
    pawnSelected = document.createElement('img'),
    kingSelected = document.createElement('img'),
    bishopSelected = document.createElement('img'),
    knightSelected = document.createElement('img'),
    queenSelected = document.createElement('img');
choice.style.margin = '60px';
rookSelected.src = 'img/rook.jpg';
pawnSelected.src = 'img/pawn.jpg';
kingSelected.src = 'img/king.jpg';
bishopSelected.src = 'img/bishop.jpg';
knightSelected.src = 'img/knight.jpg';
queenSelected.src = 'img/queen.jpg';
rookSelected.id = 'rookSelected';
kingSelected.id = 'kingSelected';
bishopSelected.id = 'bishopSelected';
queenSelected.id = 'queenSelected';
pawnSelected.id = 'pawnSelected';
knightSelected.id = 'knightSelected';

rookSelected.onmousedown = function(e) {
    move(e);
};
queenSelected.onmousedown = function(e) {
    move(e);
};
kingSelected.onmousedown = function(e) {
    move(e);
};
bishopSelected.onmousedown = function(e) {
    move(e);
};
pawnSelected.onmousedown = function(e) {
    move(e);
};
knightSelected.onmousedown = function(e) {
    move(e);
};

function move(e) {
    main.innerHTML = '';
    table();
    var oldEl = document.getElementById(e.target.id);
    var el = oldEl.cloneNode(false);
    el.name = 'moving';
    e.preventDefault();
    el.style.position = 'absolute';
    function moveTo(e) {
        el.style.left = e.pageX - el.offsetWidth / 2 + 'px';
        el.style.top = e.pageY - el.offsetHeight / 2 + 'px';
    }
    moveTo(e);
    document.body.appendChild(el);
    el.style.zIndex = 1000;
    document.onmousemove = function(e) {
        moveTo(e);
    };
    el.onmouseup = function(e) {
        var nodeList = document.getElementsByName('moving');
        var elUp = nodeList[nodeList.length - 1];
        elUp.style.display = 'none';
        var cell = document.elementFromPoint(e.clientX, e.clientY);
        switch (el.id) {
            case 'pawnSelected': pawn.call(cell);
                break;
            case 'kingSelected': king.call(cell);
                break;
            case 'queenSelected': queen.call(cell);
                break;
            case 'knightSelected': knight.call(cell);
                break;
            case 'bishopSelected': bishop.call(cell);
                break;
            case 'rookSelected': rook.call(cell)
        }
        document.onmousemove = null;
        el.onmouseup = null;
    }
}

choice.appendChild(pawnSelected);
choice.appendChild(rookSelected);
choice.appendChild(knightSelected);
choice.appendChild(bishopSelected);
choice.appendChild(queenSelected);
choice.appendChild(kingSelected);
var main = document.getElementById('contentArea');

var block;
var bw = true;
var green;
function rook() {
    this.style.backgroundColor = 'red';
    for(var i = +this.id + 1; i <= +this.id + 7 - this.id.substr(-1); i++){
        green= document.getElementById(i);
        green.style.backgroundImage = "url('img/rook.jpg')"
    }
    for(var j = this.id - 1; j >= this.id - this.id.substr(-1); j--){
        green = document.getElementById(j);
        green.style.backgroundImage = "url('img/rook.jpg')"
    }
    for(var k = this.id - 10; k >= 0; k-=10){
        green = document.getElementById(k);
        if (green) {
            green.style.backgroundImage = "url('img/rook.jpg')"
        }
    }
    for(var l = +this.id + 10; l <= 77; l+=10){
        green = document.getElementById(l);
        if (green) {
            green.style.backgroundImage = "url('img/rook.jpg')"
        }
    }
}

function pawn() {
    this.style.backgroundColor = 'red';
    green = document.getElementById(+this.id - 10 + '');
    if (green) {
        green.style.backgroundImage = "url('img/pawn.jpg')"
    }
    if (+this.id >= 60 && +this.id <= 77) {
        var green1 = document.getElementById(+this.id - 20 + '');
        green1.style.backgroundImage = "url('img/pawn.jpg')";
    }
}

function king() {
    this.style.backgroundColor = 'red';
    var allowed = [+this.id + 1 + '', +this.id - 1 + '', +this.id - 10 + '', +this.id + 10 + '', +this.id + 9 + '', +this.id + 11 + '', +this.id - 9 + '', +this.id - 11 + ''];
    allowed.forEach(function(el) {
        green = document.getElementById(el);
        if (green) {
            green.style.backgroundImage = "url('img/king.jpg')"
        }
    });
}

function bishop() {
    this.style.backgroundColor = 'red';
    for(var k = this.id - 11; k >= 0; k-=11){
        green = document.getElementById(k);
        if (green) {
            green.style.backgroundImage = "url('img/bishop.jpg')"
        } else break;
    }
    for(var l = +this.id + 11; l <= 77; l+=11){
        green = document.getElementById(l);
        if (green) {
            green.style.backgroundImage = "url('img/bishop.jpg')"
        } else break;
    }
    for(var m = this.id - 9; m >= 0; m-=9){
        green = document.getElementById(m);
        if (green) {
            green.style.backgroundImage = "url('img/bishop.jpg')"
        } else break;
    }
    for(var n = +this.id + 9; n <= 77; n+=9){
        green = document.getElementById(n);
        if (green) {
            green.style.backgroundImage = "url('img/bishop.jpg')"
        } else break;
    }
}

function knight() {
    this.style.backgroundColor = 'red';
    var allowed = [+this.id - 19 + '', +this.id - 21 + '', +this.id - 12 + '', +this.id + 8 + '', +this.id + 19 + '', +this.id + 21 + '', +this.id + 12 + '', +this.id - 8 + ''];
    allowed.forEach(function(el) {
        green = document.getElementById(el);
        if (green) {
            green.style.backgroundImage = "url('img/knight.jpg')"
        }
    });
}

function queen() {
    this.style.backgroundColor = 'red';
    for(var k = this.id - 11; k >= 0; k-=11){
        green = document.getElementById(k);
        if (green) {
            green.style.backgroundImage = "url('img/queen.jpg')"
        } else break;
    }
    for(var l = +this.id + 11; l <= 77; l+=11){
        green = document.getElementById(l);
        if (green) {
            green.style.backgroundImage = "url('img/queen.jpg')"
        } else break;
    }
    for(var m = this.id - 9; m >= 0; m-=9){
        green = document.getElementById(m);
        if (green) {
            green.style.backgroundImage = "url('img/queen.jpg')"
        } else break;
    }
    for(var n = +this.id + 9; n <= 77; n+=9){
        green = document.getElementById(n);
        if (green) {
            green.style.backgroundImage = "url('img/queen.jpg')"
        } else break;
    }
    for(var o = +this.id + 1; o <= +this.id + 7 - this.id.substr(-1); o++){
        green= document.getElementById(o);
        green.style.backgroundImage = "url('img/queen.jpg')"
    }
    for(var p = this.id - 1; p >= this.id - this.id.substr(-1); p--){
        green = document.getElementById(p);
        green.style.backgroundImage = "url('img/queen.jpg')"
    }
    for(var q = this.id - 10; q >= 0; q-=10){
        green = document.getElementById(q);
        if (green) {
            green.style.backgroundImage = "url('img/queen.jpg')"
        }
    }
    for(var r = +this.id + 10; r <= 77; r+=10){
        green = document.getElementById(r);
        if (green) {
            green.style.backgroundImage = "url('img/queen.jpg')"
        }
    }
}
function table() {
    for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 8; j++) {
            if (j === 0)
                bw = !bw;
            block = document.createElement('div');
            block.style.height = '60px';
            block.style.width = '60px';
            block.style.float = 'left';
            if (bw) {
                block.style.backgroundColor = 'black';
            } else {
                block.style.backgroundColor = 'white';
            }
            block.id = +(i + '' + j);
            main.appendChild(block);
            bw = !bw;
        }
    }
    main.appendChild(choice);
}
table();
